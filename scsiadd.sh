#!/bin/bash
# see https://codeberg.org/Windfluechter/scsiadd.sh
# 
# needs "lssci" to be installed.
# on Debian: "apt install lsscsi"
#

MODE=$1
DEV=$2

case ${MODE} in
	"scan")
		lsscsi > /tmp/lsscsi.1
		for HA in `lsscsi -H | grep -v pata | grep -v ahci | awk '{print $1}' | sed  's/\[//g' | sed 's/\]//g'`; do 
			echo "Scanning hostadapter ${HA}..."
			echo "- - -" > /sys/class/scsi_host/host${HA}/scan
		done
		lsscsi > /tmp/lsscsi.2
		echo "Added the following SCSI device(s):" 
		echo "==================================="
		comm -13 /tmp/lsscsi.1 /tmp/lsscsi.2
		echo ""
		rm /tmp/lsscsi.1 /tmp/lsscsi.2
		/etc/init.d/udev restart
		;;
	"del")
		ADDRESS=`lsscsi | grep ${DEV} | awk '{print $1}' | sed  's/\[//g' | sed 's/\]//g'`
		echo 1 > /sys/class/scsi_device/${ADDRESS}/device/delete
		lsscsi
		;;
	*)
		lsscsi
esac





