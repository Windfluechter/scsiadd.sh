# scsiadd.sh
A small shell script to make handling adding/removing SCSI disks more convenient. 

# Requirements
* `lsscsi` : Needs "lsscsi" to be installed, e.g. on Debian do a 'apt install lsscsi'.<br> 
* `comm` : Also it needs the command `comm`, but this is in package `coreutils`. So it should be always available.
* `udevd` : Of course, udev needs to be present as well, because it get's restarted to update the devices under `/dev/` path.  

# Usage
Just put in somewhere in your `$PATH`, like maybe `/usr/local/bin/`. <br>
`scsiadd.sh` - When called without parameter, it just lists the current active SCSI devices.<br>
`scsiadd.sh scan` - With option `scan` the SCSI busses are scanned for new devices that will be added. <br>
`scsiadd.sh del <device>` - The `del` option removes a SCSI device (shortname), e.g. `sdd`.<br>
 
# Examples
```
root@rabban:~# scsiadd.sh
[0:0:0:0]    cd/dvd  TOSHIBA  DVD-ROM SD-R5112 1031  /dev/sr0
[2:0:0:0]    disk    ATA      HDS725050KLA360  A51A  /dev/sda
[3:0:0:0]    disk    ATA      HDS725050KLA360  A51A  /dev/sdb
[4:0:0:0]    disk    ATA      HDS725050KLA360  A51A  /dev/sdc
[8:0:11:0]   disk    IFT      A08U-C2412       342L  /dev/sdd
[8:0:12:0]   disk    IFT      A08U-C2412       342L  /dev/sde
[8:0:12:1]   disk    IFT      A08U-C2412       342L  /dev/sdf
root@rabban:~# scsiadd.sh del sde
[0:0:0:0]    cd/dvd  TOSHIBA  DVD-ROM SD-R5112 1031  /dev/sr0
[2:0:0:0]    disk    ATA      HDS725050KLA360  A51A  /dev/sda
[3:0:0:0]    disk    ATA      HDS725050KLA360  A51A  /dev/sdb
[4:0:0:0]    disk    ATA      HDS725050KLA360  A51A  /dev/sdc
[8:0:11:0]   disk    IFT      A08U-C2412       342L  /dev/sdd
[8:0:12:1]   disk    IFT      A08U-C2412       342L  /dev/sdf
```

# Donations
If you want to support my work, you can donate via LiberaPay: 

[![Donate via LiberaPay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/Windfluechter/donate)
